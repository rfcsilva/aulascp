#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

double getRandomVal(){

    return ( (double)  rand() / RAND_MAX ) * 2.0-1.0;//float in range -1 to 1

}

int hitCounter(int shots )
{
	int hits = 0;

  for(int i = 0; i < shots; i++)
  {
	   double x = getRandomVal();
     double y = getRandomVal();
     double sqrt_val = sqrt( x*x + y*y );
     if( sqrt_val <= 1.0 )
        hits++;
  }

  return hits;
}


int main(int argc, char const *argv[])
{

	int totalShots = atoi(argv[1]);
	int hits = hitCounter(totalShots);
	double ratio = (double) hits/totalShots;
  double aproxPi = 4.0 * ratio;

	printf("ApproxPi: %f\n", aproxPi );

	/* code */
	return 0;
}
