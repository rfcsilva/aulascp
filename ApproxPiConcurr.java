
public class ApproxPiConcurr{

    public static HitCounter[] threads_arr;

  public static void main(String[] args) throws Exception {

    int numbExcecutuions = Integer.parseInt(args[0]);
    int threads = Integer.parseInt(args[1]);
    threads_arr = new HitCounter[threads];

    startExcec( numbExcecutuions, threads );
    int hits = finishExec();
    double sol = 4* (double) hits/numbExcecutuions;
    printSol( numbExcecutuions, hits, sol );

  }

  private static void startExcec(int numbExcecutuions, int threads){

    int threadExecs = numbExcecutuions / threads;
    for(int i = 0; i<threads; i++){
      HitCounter thread = new HitCounter(threadExecs );
      thread.start();
      threads_arr[i] = thread;
      }
    }

    private static int finishExec() throws  Exception{

      int totalHits = 0;

      for(HitCounter myThread : threads_arr){
          myThread.join();
          totalHits+= myThread.getHits();
      }

      return totalHits;
    }

    private static void printSol(int numbExcecutuions, int hits , double sol){
    System.out.println("Total Number of points: " + numbExcecutuions);
    System.out.println("Points within circle: " + hits);
    System.out.println("Pi: " + sol);

  }

}
