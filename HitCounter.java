import java.util.concurrent.ThreadLocalRandom;
public class HitCounter extends Thread {

  private int numbExcecutuions;
  private int hits;
  private ThreadLocalRandom tlr;

  public HitCounter(int numbExcecutuions){
    this.numbExcecutuions = numbExcecutuions;
    hits = 0;
    tlr =  ThreadLocalRandom.current();
  }

  public void run() {

    for(int i=0; i < numbExcecutuions; i++){

      double x = getRandomVal();
      double y = getRandomVal();
      double sqrt = Math.sqrt( x*x + y*y );

      if( sqrt <= 1.0 )
        hits++;

    }

  }

  public double getRandomVal(){
    return tlr.nextDouble();
  }

   public int getHits(){
     return hits;
   }
}
