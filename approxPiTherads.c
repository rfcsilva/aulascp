#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <math.h>

typedef struct _ThreadArg {
   int shots;
   unsigned int* seed;

} ThreadArg;


double getRandomVal(unsigned int* state){
    return ( (double)  rand_r(state) / RAND_MAX ) * 2.0-1.0;//float in range -1 to 1
}

void* hitCounter(void * _args)
{
  unsigned int seed = time(NULL);
  int * hits = malloc(sizeof(int));
  *hits = 0;
  ThreadArg args = *(ThreadArg*) _args;
  for(int i=0; i < args.shots; i++){

    double y = getRandomVal(&seed);
    double x = getRandomVal(&seed);
    double sqrt_val = sqrt( x*x + y*y );
    if( sqrt_val <= 1.0 )
      (*hits)++;
  }
  return (void*) hits;
}


int main( int argc, char *argv[] )
{
    if(argc != 3){
      printf("Usage: ./ApproxiThreads numbShots numbThreads\n");
      exit(1);
    }

    int numbShots = atoi(argv[1]);
    int numbThreads = atoi(argv[2]);
    int threadShots = numbShots / numbThreads;
    int totalHits = 0;
    double ratio, pi;


    //Initialize threads
    ThreadArg threadArgsArr[numbThreads];
    pthread_t threads[numbThreads];
    for(int i = 0; i<numbThreads; i++){

      ThreadArg thread;
      thread.shots = threadShots;
      threadArgsArr[i] = thread;

      pthread_create(&threads[i], NULL, hitCounter, (void*) &thread);

    }

    //Join threads
      for(int j = 0; j<numbThreads; j++){

        void* result;
        pthread_join(threads[j], &result);
        totalHits+= (*(int*) result);
        free(result);
    }

    ratio = (double) totalHits / numbShots;
    pi = 4.0 * ratio;
    printf("ApproxiPi:  %f\n", pi);
    exit(0);
}
