
public class ApproxPi{

  public static void main(String[] args) {

    int numbExcecutuions = Integer.parseInt(args[0]);


    int hits = 0;
    long start = System.currentTimeMillis();

    for(int i=0; i < numbExcecutuions; i++){

      double x = getRandomVal();
      double y = getRandomVal();
      double sqrt = Math.sqrt( x*x + y*y );

      if( sqrt <= 1.0 )
        hits++;

    }

    double ratio = (double) hits/numbExcecutuions;
    double sol = 4.0 * ratio;


    printSol( numbExcecutuions, hits, sol );

  }


  private static double getRandomVal(){
    return Math.random() * 2.0 - 1.0;
  }

  private static void printSol(int numbExcecutuions, int hits , double sol){
    System.out.println("Total Number of points: " + numbExcecutuions);
    System.out.println("Points within circle: " + hits);
    System.out.println("Pi: " + sol);


  }

}
